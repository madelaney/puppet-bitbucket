define bitbucket::instance(
  $version,
  $user           = $::bitbucket::user,
  $baseurl        = $::bitbucket::baseurl,
  $install_root   = $::bitbucket::install_root,
  $bitbucket_home = $::bitbucket::bitbucket_home,
  $current        = false,
  $cleanup        = false
) {
  $archive = "atlassian-bitbucket-${version}.tar.gz"

  archive {
    $archive:
      source       => "${baseurl}/${archive}",
      path         => "${install_root}/${archive}",
      extract      => true,
      extract_path => $install_root,
      cleanup      => $cleanup,
      user         => $user
  }

  if $current {
    file_line {
      "start-append-jre-bitbucket-home-${version}":
        path    => "${install_root}/current/bin/start-bitbucket.sh",
        line    => "source ${install_root}/set-env.sh",
        after   => '#!/usr/bin/env bash',
        require => File["${install_root}/current"];

      "stop-append-jre-bitbucket-home-${version}":
        path    => "${install_root}/current/bin/stop-bitbucket.sh",
        line    => "source ${install_root}/set-env.sh",
        after   => '#!/usr/bin/env bash',
        require => File["${install_root}/current"];
    }

    file {
      "${install_root}/current":
        ensure => link,
        target => "${install_root}/atlassian-bitbucket-${version}";
    }
  }
}