class bitbucket::config(
  $baseurl        = $::bitbucket::baseurl,
  $install_root   = $::bitbucket::install_root,
  $user           = $::bitbucket::user,
  $manage_user    = $::bitbucket::manage_user,
  $bitbucket_home = $::bitbucket::bitbucket_home,
  $properties     = $::bitbucket::properties,
  $instances      = $::bitbucket::instances,
  $java_home      = $::bitbucket::java_home
) {

  $_shell = $::osfamily ? {
    'RedHat'  => '/usr/bin/sh',
    default   => '/bin/sh'
  }

  user {
    $user:
      ensure => present,
      home   => $bitbucket_home,
      shell  => $_shell;
  }

  exec {
    'make bitbucket home path':
      command => "/bin/mkdir -vp ${bitbucket_home}",
      creates => $bitbucket_home,
      path    => ['/bin'];
  }

  file {
    $install_root:
      ensure  => directory,
      owner   => $user,
      mode    => '0750',
      require => User[$user];

    "${install_root}/set-env.sh":
      ensure  => file,
      owner   => $user,
      mode    => '0750',
      content => template('bitbucket/set-env.sh.erb'),
      require => File[$install_root];

    $bitbucket_home:
      ensure  => directory,
      owner   => $user,
      mode    => '0750',
      require => [User[$user], Exec['make bitbucket home path']];

    "${bitbucket_home}/shared":
      ensure  => directory,
      owner   => $user,
      mode    => '0750',
      require => File[$bitbucket_home];

    "${bitbucket_home}/shared/bitbucket.properties":
      ensure  => file,
      owner   => $user,
      mode    => '0600',
      content => template('bitbucket/bitbucket.properties.erb');
  }

  $_instance_defaults = {
    require => File[$install_root]
  }

  create_resources('bitbucket::instance', $instances, $_instance_defaults)
}
