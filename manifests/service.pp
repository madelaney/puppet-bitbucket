class bitbucket::service(
  $rc_dir         = $::bitbucket::rc_dir,
  $install_root   = $::bitbucket::install_root,
  $bitbucket_home = $::bitbucket::bitbucket_home,
  $user           = $::bitbucket::user,
  $java_home      = $::bitbucket::java_home
) {
  require ::bitbucket

  debug("OS Family is ${::osfamily}")

  file {
    "${rc_dir}/bitbucket":
      ensure  => file,
      mode    => '0750',
      content => template("bitbucket/bitbucket.${::osfamily}.erb");
  }

  service {
    'bitbucket':
      ensure    => running,
      enable    => true,
      require   => File["${rc_dir}/bitbucket"],
      subscribe => File["${bitbucket_home}/shared/bitbucket.properties"];
  }
}
