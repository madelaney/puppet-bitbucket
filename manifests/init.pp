# Class: bitbucket
# ===========================
#
# Full description of class bitbucket here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'bitbucket':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2017 Your name here, unless otherwise noted.
#
class bitbucket(
  $baseurl        = $::bitbucket::params::baseurl,
  $install_root   = $::bitbucket::params::install_root,
  $user           = $::bitbucket::params::user,
  $manage_user    = true,
  $bitbucket_home = $::bitbucket::params::bitbucket_home,
  $rc_dir         = $::bitbucket::params::rc_dir,
  $properties     = {},
  $instances      = {},
  $java_home      = $::bitbucket::params::java_home
) inherits ::bitbucket::params {
  class { '::bitbucket::config': } ->
  class { '::bitbucket::service': }
}
