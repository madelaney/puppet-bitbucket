class bitbucket::params() {
  $baseurl = 'https://www.atlassian.com/software/stash/downloads/binary'
  $bitbucket_home = '/opt/bitbucket-home'
  $install_root = '/usr/local/atlassian-bitbucket'
  $user = 'bitbucket'

  case $::osfamily {
    'FreeBSD': {
      $java_home = '/usr/local/openjdk8'
      $rc_dir = '/usr/local/etc/rc.d'
    }
    default: {
      $java_home = '/usr/java/default'
      $rc_dir = '/etc/init.d'
    }
  }
}