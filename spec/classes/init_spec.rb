require 'spec_helper'
describe 'bitbucket' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) { facts }
      context 'with default values for all parameters' do
        it { should contain_class('bitbucket') }
      end
    end
  end
end
