require 'spec_helper_acceptance'

describe 'bitbucket class' do
  context 'with default parameters' do
    if default['platform'] =~ /el-7-x86_64/
      it 'should work with no errors on centos' do
        pp = <<-EOS
        java::oracle {
          'jdk8' :
            ensure  => 'present',
            version => '8',
            java_se => 'jdk',
        } ->
        class {
          'bitbucket':
            instances => {
              '5.3.3' => {
                version => '5.3.3'
              },
              '5.2.5' => {
                version => '5.2.5',
                current => true
              }
            },
            properties => {
              'hello' => 'world'
            }
        }
        EOS

        # Run it twice and test for idempotency
        apply_manifest(pp, catch_failures: true)
        apply_manifest(pp, catch_changes: true)

      end

    elsif default['platform'] =~ /freebsd/
      it 'should work with no errors on freebsd' do
        pp = <<-EOS
        package {
          ['openjdk8', 'bash']:
            ensure => present,
            before => Class['bitbucket'];
        } ->
        file_line {
          'procfs':
            path => '/etc/fstab',
            line => 'proc	/proc	procfs	rw	0	0';
          'fdescfs':
            path => '/etc/fstab',
            line => 'fdesc	/dev/fd	fdescfs	rw	0	0';
        } ->
        exec {
          'mount':
            command     => '/sbin/mount -a',
            refreshonly => true,
            subscribe   => File_line['procfs', 'fdescfs'];
        } ->
        class {
          'bitbucket':
            instances => {
              '5.3.3' => {
                version => '5.3.3'
              },
              '5.2.5' => {
                version => '5.2.5',
                current => true
              }
            },
            properties => {
              'hello' => 'world'
            }
        }
        EOS

        # Run it twice and test for idempotency
        apply_manifest(pp, catch_failures: true)
        apply_manifest(pp, catch_changes: true)
      end
    else
      it 'should work with no errors on ubuntu' do
        pp = <<-EOS
        package {
          'openjdk-8-jdk':
            ensure => present,
            before => Class['bitbucket'];
        } ->
        class {
          'bitbucket':
            instances => {
              '5.3.3' => {
                version => '5.3.3'
              },
              '5.2.5' => {
                version => '5.2.5',
                current => true
              }
            },
            properties => {
              'hello' => 'world'
            }
        }
        EOS

        # Run it twice and test for idempotency
        apply_manifest(pp, catch_failures: true)
        apply_manifest(pp, catch_changes: true)

      end
    end

    describe service('bitbucket') do
      it { is_expected.to be_enabled }
      it { is_expected.to be_running }
    end
  end
end
